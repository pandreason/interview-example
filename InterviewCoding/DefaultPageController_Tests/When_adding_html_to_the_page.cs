﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.UI;
using InterviewCoding;

namespace DefaultPageController_Tests
{
    [TestClass]
    public class When_adding_html_to_the_page
    {
        [TestMethod]
        public void Then_the_html_is_added()
        {
            //Arrange
            var testElement = "<p id='myTestElement'>";
            var testControl = new Control();
            var testControlCollection = new ControlCollection(testControl);

            var pageController = new DefaultPageController();

            //Act
            pageController.AddHTMLToPage(testElement, testControlCollection, "myTestElement");

            //Assert
            Assert.IsTrue(testControlCollection.Count == 1);
        }
    }
}
