﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.UI.WebControls;
using InterviewCoding;

namespace DefaultPageController_Tests
{
    [TestClass]
    public class When_creating_a_button
    {
        private const string TestId = "myTestId";
        private const string TestTitle = "myTestTitle";
        private const string TestCommandName = "myTestCommandName";

        private CommandEventHandler _testEventHandler;
        private DefaultPageController _testController;

        void testDelegateCommand(Object sender, CommandEventArgs e) { }
        void dummyDelegateCommand(Object sender, CommandEventArgs e) { }

        [TestInitialize]
        public void Initialize()
        {
            //Arrange
            _testController = new DefaultPageController();
            _testEventHandler = new CommandEventHandler(testDelegateCommand);
        }
        [TestMethod]
        public void Then_the_id_is_added()
        {
            //Act
            var testButton = _testController.CreateButton(TestId, "dummyTitle", "dummyCommandName", new CommandEventHandler(dummyDelegateCommand));

            //Assert
            Assert.AreEqual(TestId, testButton.ID);
        }

        [TestMethod]
        public void Then_the_title_is_added()
        {
            //Act
            var testButton = _testController.CreateButton("dummyId", TestTitle, "dummyCommandName", new CommandEventHandler(dummyDelegateCommand));

            //Assert
            Assert.AreEqual(TestTitle, testButton.Text);
        }

        [TestMethod]
        public void Then_the_command_name_is_added()
        {
            //Act
            var testButton = _testController.CreateButton("dummyId", "dummyTitle", TestCommandName, new CommandEventHandler(dummyDelegateCommand));

            //Assert
            Assert.AreEqual(TestCommandName, testButton.CommandName);
        }
    }
}
