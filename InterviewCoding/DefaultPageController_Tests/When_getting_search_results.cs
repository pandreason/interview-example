﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using InterviewCoding;
using System.Linq;

namespace DefaultPageController_Tests
{
    [TestClass]
    public class When_getting_search_results
    {
        private DataTable _table;

        private DefaultPageController _pageController;

        [TestInitialize]
        public void Initialize()
        {
            _pageController = new DefaultPageController();
            _table = new DataTable();
        }

        [TestMethod]
        public void And_there_are_two_columns_then_the_resulting_html_has_2_headers()
        {
            _table.Columns.Add("lastname", typeof(string));
            _table.Columns.Add("Date", typeof(DateTime));

            _table.Rows.Add("cat", DateTime.Now);
            _table.Rows.Add("dog", DateTime.Today);

            var output = _pageController.GetResults("cat", _table);

            var indexesOfHeaderElements = output.AllIndexesOf("<th>");

            Assert.IsTrue(indexesOfHeaderElements.Count == 2);

        }

        [TestMethod]
        public void And_there_are_five_columns_then_the_resulting_html_has_5_headers()
        {
            _table.Columns.Add("lastname", typeof(string));
            _table.Columns.Add("Date", typeof(DateTime));
            _table.Columns.Add("Name", typeof(string));
            _table.Columns.Add("timezone", typeof(string));
            _table.Columns.Add("numberofarms", typeof(int));

            _table.Rows.Add("cat", DateTime.Now);
            _table.Rows.Add("dog", DateTime.Today);

            var output = _pageController.GetResults("cat", _table);

            var indexesOfHeaderElements = output.AllIndexesOf("<th>");

            Assert.IsTrue(indexesOfHeaderElements.Count == 5);

        }

        [TestMethod]
        public void And_there_are_five_matching_rows_then_the_resulting_html_has_5_rows()
        {
            _table.Columns.Add("lastname", typeof(string));
            _table.Columns.Add("Date", typeof(DateTime));
            
            _table.Rows.Add("cat", DateTime.Now);
            _table.Rows.Add("dog", DateTime.Today);
            _table.Rows.Add("dog", DateTime.Today);
            _table.Rows.Add("dog", DateTime.Today);
            _table.Rows.Add("dog", DateTime.Today);
            _table.Rows.Add("dog", DateTime.Today);

            var output = _pageController.GetResults("dog", _table);

            var indexesOfRowElements = output.AllIndexesOf("<tr>");

            Assert.IsTrue(indexesOfRowElements.Count() == 5);
        }

        [TestMethod]
        public void And_there_is_one_matching_row_then_the_resulting_html_has_1_row()
        {
            _table.Columns.Add("lastname", typeof(string));
            _table.Columns.Add("Date", typeof(DateTime));

            _table.Rows.Add("cat", DateTime.Now);
            _table.Rows.Add("dog", DateTime.Today);
            _table.Rows.Add("bird", DateTime.Today);
            _table.Rows.Add("frog", DateTime.Today);
            _table.Rows.Add("fish", DateTime.Today);
            _table.Rows.Add("salamander", DateTime.Today);
            _table.Rows.Add("elephant", DateTime.Today);

            var output = _pageController.GetResults("dog", _table);

            //skip the header <tr>
            var indexesOfRowElements = output.AllIndexesOf("<tr>");

            Assert.IsTrue(indexesOfRowElements.Count() == 1);
        }
    }
}
