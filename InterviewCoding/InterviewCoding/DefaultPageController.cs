﻿using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InterviewCoding
{
    public class DefaultPageController
    {
        public void ClearReults(Control control, string searchTextId)
        {
            var textboxControl = control.FindControl(searchTextId) as TextBox;
            textboxControl.Text = string.Empty;
            control.Controls.Remove(control.FindControl("output"));
            control.Controls.Remove(control.FindControl("noResults"));
        }

        public string GetResults(string searchText, DataTable dt)
        {
            List<string> headers = new List<string>();

            var results = dt.Select("lastname ='" + searchText + "'");
            foreach (DataColumn column in dt.Columns)
            {
                headers.Add("<th>" + column.ColumnName.UppercaseFirst() + "</th>");
            }

            string resultsOutput = string.Empty;

            if (results.Length == 0)
                resultsOutput += "<p id='noResults'>No Results Found</p>";
            else
            {
                resultsOutput += "<table id='output'>";


                foreach (var header in headers)
                    resultsOutput += header;

                foreach (var row in results)
                {
                    resultsOutput += "<tr>";
                    foreach (DataColumn column in row.Table.Columns)
                    {
                        resultsOutput += "<td>" + row[column].ToString() + "</td>";
                    }
                    resultsOutput += "</tr>";
                }
                resultsOutput += "</table>";
            }

            return resultsOutput;
        }


        //Create a button control with a specified ID, Title, and Command
        public Button CreateButton(string id, string Title, string command, CommandEventHandler eventHandler)
        {
            Button a = new Button();
            a.ID = id;
            a.Text = Title;
            a.CommandName = command;
            a.Command += eventHandler;
            a.CssClass = "Blue";
            return a;
        }

        //Adds a specified HTML literal to the page
        public void AddHTMLToPage(string htmlCode, ControlCollection controlCollection, string id = null)
        {
            var control = new LiteralControl(htmlCode);
            control.ID = id;
            controlCollection.Add(control);
        }
    }
}