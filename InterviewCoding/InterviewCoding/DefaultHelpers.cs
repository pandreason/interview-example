﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.IO;
using System.Reflection;


namespace InterviewCoding
{
    public static class DefaultHelpers
    {
        //Method to read an xml file into datatable provided
        public static DataTable ReadXML(string path)
        {
            //create the DataTable that will hold the data
            DataSet ds = new DataSet("XmlData");
            try
            {
                //open the file using a Stream
                using (Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    //use ReadXml to read the XML stream
                    ds.ReadXml(stream);

                    //return the results
                    return ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                return ds.Tables[0];
            }
        }


    }
}