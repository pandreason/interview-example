﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="InterviewCoding._Default" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        input.Blue{
            margin-top: 15px;
            background-color:blue;
            color:yellow;
        }
        input.Red{
            background-color:red;
            color:blue;
        }
        input[type=text]{
            margin-top: 15px;
        }
        #output {
            margin-top: 20px
        }
        #output th {
            padding: 5px;
            border: solid 1px black;
            font-weight: bold;
        }
        #output td {
            padding: 5px;
            border: solid 1px black;
        }
        #noResults {
            font-size: 24px;
        }
    </style>
    <asp:Panel ID="pnlFormStuff" runat="server" Width="100%">
        <asp:TextBox runat="server" id="search" placeholder="Enter Last Name"/> 
    </asp:Panel>
</asp:Content>
