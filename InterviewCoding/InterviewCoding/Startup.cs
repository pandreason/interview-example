﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InterviewCoding.Startup))]
namespace InterviewCoding
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
