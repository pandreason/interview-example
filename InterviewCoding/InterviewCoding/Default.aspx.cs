﻿using System;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;


namespace InterviewCoding
{
    public partial class _Default : Page
    {
        private static DefaultPageController _controller;
        protected void Page_Init(object sender, EventArgs e)
        {
            _controller = new DefaultPageController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var findButton = _controller.CreateButton("find", "Find", "Find", new CommandEventHandler(CommandBtn_Click));
            var clearButton = _controller.CreateButton("clear", "Clear", "Clear", new CommandEventHandler(CommandBtn_Click));
            pnlFormStuff.Controls.Add(findButton);
            pnlFormStuff.Controls.Add(clearButton);
        }


        //Handles Button Click Events
        void CommandBtn_Click(Object sender, CommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "Find":
                    //Provided code for data access
                    DataTable dt = DefaultHelpers.ReadXML(Server.MapPath("ClientSample.xml"));
                    var searchResultsHtml = _controller.GetResults(search.Text, dt);
                    _controller.AddHTMLToPage(searchResultsHtml, pnlFormStuff.Controls);
                    break;

                case "Clear":
                    _controller.ClearReults(pnlFormStuff, "search");
                    break;
            }
        }
    }
}